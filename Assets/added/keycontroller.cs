using UnityEngine;
using System.Collections;

public class keycontroller : MonoBehaviour {

    public float moveSpeed = 7.0f;
    public float mouseSensitivity = 3.0f;
    public float pitchLimit = 80;

    private float pitch = 0;

	// Use this for initialization
	void Start () {
        Screen.lockCursor = true;
	}
	
	// Update is called once per frame
	void Update () {
        
        float yaw = mouseSensitivity * Input.GetAxis("Mouse X");
        transform.Rotate(0, yaw , 0);

        float pitchDiff = mouseSensitivity * Input.GetAxis("Mouse Y");
        pitch -= pitchDiff;
        pitch = Mathf.Clamp(pitch, -pitchLimit, pitchLimit);
        Camera.main.transform.localRotation = Quaternion.Euler(pitch, yaw, 0);

        float forwardSpeed = moveSpeed * Input.GetAxis("Vertical");
        float strafeSpeed = moveSpeed * Input.GetAxis("Horizontal");

        Vector3 speed = new Vector3(strafeSpeed, 0, forwardSpeed);
        speed = transform.rotation * speed;

        CharacterController cc = GetComponent<CharacterController>();
        cc.SimpleMove(speed);
	}
}
